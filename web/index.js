import { join } from "path";
import { readFileSync } from "fs";
import express from "express";
import serveStatic from "serve-static";
// import {registerScriptTag} from "./scripttag.js"
import shopify from "./shopify.js";
import productCreator from "./product-creator.js";
import GDPRWebhookHandlers from "./gdpr.js";
import { ThemeForm, GetThemeData } from './db.js'
import cors from 'cors'
import { billingConfig } from "./shopify.js";
import fetch from 'node-fetch';
const HOST = process.env.HOST;

const PORT = parseInt(process.env.BACKEND_PORT || process.env.PORT || 3000);

const STATIC_PATH =
  process.env.NODE_ENV === "production"
    ? `${process.cwd()}/frontend/dist`
    : `${process.cwd()}/frontend/`;

const app = express();
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  next();
});
app.use(cors())
// Set up Shopify authentication and webhook handling

app.get(shopify.config.auth.path, shopify.auth.begin());
app.get(
  shopify.config.auth.callbackPath,
  shopify.auth.callback(),
  async (req, res, next) => {
    const plans = Object.keys(billingConfig);
    const session = res.locals.shopify.session;
    const hasPayment = await shopify.api.billing.check({
      session,
      plans: plans[0],
      isTest: true,
    });

    if (hasPayment) {
      next();
    } else {
      const resp = await shopify.api.billing.request({
        session,
        plan: plans[0],
        isTest: true,
      })
        res.redirect(resp);    
    }
  },
);


// This can happen at any point after the merchant goes through the OAuth process, as long as there is a session object
// The session can be retrieved from storage using the session id returned from shopify.session.getCurrentId


app.post(
  shopify.config.webhooks.path,
  shopify.processWebhooks({ webhookHandlers: GDPRWebhookHandlers }),
);

app.get("/api/getthemedata", async (req, res) => {
  try {
    const { shop } = req.query;
    const response = await GetThemeData(shop);
    res.send(response[0])
  } catch (error) {
    console.log("/api/getthemedata : ", error.message);
  }
})


// If you are adding routes outside of the /api path, remember to
// also add a proxy rule for them in web/frontend/vite.config.js

app.use("/api/*", shopify.validateAuthenticatedSession());

app.use(express.json());
app.get("/api/deleteApp", async (req, res, next) => {
  const session = res.locals.shopify.session;
  const plans = Object.keys(billingConfig);
  const hasPayment = await shopify.api.billing.check({
    session,
    plans:plans[0],
    isTest: true,
  });
  if (hasPayment === true) {
    next();
  } 
  else {
    const resp = await fetch(`https://${session.shop}/admin/api_permissions/current.json`,{
      method:"DELETE",
      headers:{
        "X-Shopify-Access-Token": session.accessToken
      }
    })
    res.status(200).json({ message: "app is deleted" });    
  }
})




app.get("/api/theme", async (req, res) => {


  const { shop, accessToken } = res.locals.shopify.session;
  await registerScriptTag(shop, accessToken)


  async function registerScriptTag(shop, accessToken, remove = false) {
    const ourScriptSrc = `${HOST}/pages/product.js`;
    console.log("SRC : ", ourScriptSrc)
    // Session is built by the OAuth process

    // Specify the name of the template the app will integrate with
    const APP_BLOCK_TEMPLATES = ['product', 'collection', 'index'];
    const session = res.locals.shopify.session
    const client = new shopify.api.clients.Rest({
      session,
    });
    const { body: { themes } } = await client.get({
      path: 'themes',
    });

    // Find the published theme
    const publishedTheme = themes.find((theme) => theme.role === 'main');
    console.log(publishedTheme);
    const { body: { assets } } = await client.get({
      path: `themes/${publishedTheme.id}/assets`
    });
    // console.log("assets", assets)
    const templateJSONFiles = assets.filter((file) => {
      return APP_BLOCK_TEMPLATES.some(template => file.key === `templates/${template}.json`);
    })


    const templateMainSections = (await Promise.all(templateJSONFiles.map(async (file, index) => {
      let acceptsAppBlock = false;
      const { body: { asset } } = await client.get({
        path: `themes/${publishedTheme.id}/assets`,
        query: { "asset[key]": file.key }
      })

      const json = JSON.parse(asset.value)

      const main = Object.entries(json.sections).find(([id, section]) => id === 'main' || section.type.startsWith("main-"))
      if (main) {
        return assets.find((file) => file.key === `sections/${main[1].type}.liquid`);
      }
    }))).filter((value) => value)
    // res.send("HELLO WORLD")
    res.status(200).send(templateMainSections)
    const sectionsWithAppBlock = (await Promise.all(templateMainSections.map(async (file, index) => {
      let acceptsAppBlock = false;
      const { body: { asset } } = await client.get({
        path: `themes/${publishedTheme.id}/assets`,
        query: { "asset[key]": file.key }
      })

      const match = asset.value.match(/\{\%\s+schema\s+\%\}([\s\S]*?)\{\%\s+endschema\s+\%\}/m)
      const schema = JSON.parse(match[1]);

      if (schema && schema.blocks) {
        acceptsAppBlock = schema.blocks.some((b => b.type === '@app'));
      }
      return acceptsAppBlock ? file : null
    }))).filter((value) => value)

    if (templateJSONFiles.length > 0 && (templateJSONFiles.length === sectionsWithAppBlock.length)) {
      // console.log('All desired templates have main sections that support app blocks!');

    }

    else if (sectionsWithAppBlock.length) {
      console.log('Only some of the desired templates support app blocks.');
    }

    else {
      console.log("None of the desired templates support app blocks");

      const { body: body2 } = await client.post({
        path: "script_tags",
        data: {
          script_tag: {
            event: "onload",
            src: ourScriptSrc,
            display_scope: "online_store",
          },
        },
        // type: DataType.JSON,
      });

      return Promise.resolve(body2.script_tag);
    }
  }

})

app.post("/api/themeformdata", async (req, res) => {
  const { shop } = res.locals.shopify.session
  const { title1, link1, url1,
    title2, link2, url2,
    title3, link3, url3,
    title4, link4, url4,
    title5, link5, url5,
    title6, link6, url6 } = req.body;
  await ThemeForm(shop, title1, link1, url1,
    title2, link2, url2,
    title3, link3, url3,
    title4, link4, url4,
    title5, link5, url5,
    title6, link6, url6)
  res.send("ok")
})


app.get("/api/products/count", async (_req, res) => {
  const countData = await shopify.api.rest.Product.count({
    session: res.locals.shopify.session,
  });
  res.status(200).send(countData);
});

app.get("/api/products/create", async (_req, res) => {
  let status = 200;
  let error = null;

  try {
    await productCreator(res.locals.shopify.session);
  } catch (e) {
    console.log(`Failed to process products/create: ${e.message}`);
    status = 500;
    error = e.message;
  }
  res.status(status).send({ success: status === 200, error });
});

app.use(serveStatic(STATIC_PATH, { index: false }));

app.use("/*", shopify.ensureInstalledOnShop(), async (_req, res, _next) => {
  return res
    .status(200)
    .set("Content-Type", "text/html")
    .send(readFileSync(join(STATIC_PATH, "index.html")));
});

app.listen(PORT);