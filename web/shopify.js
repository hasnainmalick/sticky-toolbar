import { BillingInterval, LATEST_API_VERSION, BillingReplacementBehavior } from "@shopify/shopify-api";
import { shopifyApp } from "@shopify/shopify-app-express";
import { SQLiteSessionStorage } from "@shopify/shopify-app-session-storage-sqlite";
import { restResources } from "@shopify/shopify-api/rest/admin/2023-01";
import { MySQLSessionStorage } from "@shopify/shopify-app-session-storage-mysql";
import * as dotenv from "dotenv";
dotenv.config();
const DB_USER = process.env.DB_USER;
const DB_NAME = process.env.DB_NAME;
const DB_PASS = process.env.DB_PASS;
const DB_HOST = process.env.DB_HOST;
const DB_PATH = `mysql://${DB_USER}:${DB_PASS}@127.0.0.1/${DB_NAME}`;



// The transactions with Shopify will always be marked as test transactions, unless NODE_ENV is production.
// See the ensureBilling helper to learn more about billing in this template.
export const billingConfig = {
  "Toolbar monthly charges": {
    interval: BillingInterval.Every30Days,
    amount: 7, // Monthly amount: $7
    currencyCode: 'USD',
    replacementBehavior: BillingReplacementBehavior.ApplyImmediately
  },
  "Annual charges": {
    interval: BillingInterval.Annual,
    amount: 60, // Annual amount: $60
    currencyCode: 'USD',
    replacementBehavior: BillingReplacementBehavior.ApplyImmediately
  }
};

const shopify = shopifyApp({
  api: {
    apiKey: process.env.SHOPIFY_API_KEY,
    apiSecretKey: process.env.SHOPIFY_API_SECRET,
    apiVersion: LATEST_API_VERSION,
    scopes: process.env.SCOPES.split(","),
    restResources,
    billing: billingConfig, // or replace with billing Config above to enable example billing
  },
  auth: {
    path: "/api/auth",
    callbackPath: "/api/auth/callback",
  },
  webhooks: {
    path: "/api/webhooks",
  },
  // This should be replaced with your preferred storage strategy
  sessionStorage: new MySQLSessionStorage(DB_PATH),
});

export default shopify;
