import {
  Card,
  Page,
  Layout,
  TextContainer,
  Image,
  Stack,
  Link,
  Heading,
} from "@shopify/polaris";
// import { TitleBar } from "@shopify/app-bridge-react";
import StickyTabs from "../components/Tabs";
import { useAuthenticatedFetch } from "../hooks";
import React, { useEffect } from "react";
export default function HomePage() {
  const fetch = useAuthenticatedFetch();
  const [app,setApp] =React.useState("");
  
  // window.location.reload();
  useEffect(() => {
    fetch("/api/deleteApp")
    .then((response) => response.json())
    .then((data) => {
      // console.log(data); // Log the response data received from the server
      // console.log(data)
      alert(data.message)
      window.location.reload();
      // window.location.assign("www.admin.shopify.com");
      })
  }, []);

  return (
    <Page narrowWidth>
      {/* <TitleBar title="Sticky Toolbar App" primaryAction={null} /> */}
      <Layout>
        <Layout.Section>
          <Card sectioned>
            <Stack>
              <Stack.Item fill>
                <StickyTabs />
              </Stack.Item>
            </Stack>
          </Card>
        </Layout.Section>
      </Layout>
    </Page>
  );
}
