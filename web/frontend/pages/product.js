async function Get_Data() {
    const shop = Shopify.shop;
    const response = await fetch(`https://sticky-toolbar.helpify24.com/api/getthemedata?shop=${shop}`)
    const data = await response.json();
    const mediaQuery = "(max-width: 700px)";

    if (window.matchMedia(mediaQuery).matches && data !== undefined) {
        var style = document.createElement("style");
        document.head.appendChild(style);

        style.setAttribute('media', mediaQuery);

        style.sheet.insertRule(".parent-container { display: flex; color: #000; flex-wrap: wrap; align-items: center; justify-content: space-evenly; position: fixed; bottom: 0; z-index: 999; width: 100vw; height: 50px; background-color: white; }", 0);
        style.sheet.insertRule(".toolbar-child a{ display: flex; flex-direction:column; align-items: center; justify-content: center;}", 0);
        style.sheet.insertRule(".toolbar-child img { width: 50px; height: 30px;  }", 0);

        var parentContainer = document.createElement('div');
        parentContainer.classList.add('parent-container');
        parentContainer.setAttribute('id', 'container-toolbar');
        document.body.appendChild(parentContainer);

        // first image
        var childContainer = document.createElement('div');
        childContainer.classList.add('toolbar-child');
        childContainer.setAttribute('id', 'child-' + i);
        parentContainer.appendChild(childContainer);
        var anchorTag = document.createElement('a');
        anchorTag.setAttribute('href', data.linkurl1);
        var imageTag = document.createElement('img');
        imageTag.setAttribute('src', data.link1);
        imageTag.setAttribute('alt', 'example image');
        var textNode = document.createTextNode(data.title1);
        anchorTag.appendChild(imageTag);
        anchorTag.appendChild(textNode);
        childContainer.appendChild(anchorTag);
        
        // Second image
        if(data.link2 != ""){
        var childContainer = document.createElement('div');
        childContainer.classList.add('toolbar-child');
        childContainer.setAttribute('id', 'child-' + i);
        parentContainer.appendChild(childContainer);
        var anchorTag = document.createElement('a');
        anchorTag.setAttribute('href', data.linkurl2);
        var imageTag = document.createElement('img');
        imageTag.setAttribute('src', data.link2);
        imageTag.setAttribute('alt', 'example image');
        var textNode = document.createTextNode(data.title2);
        anchorTag.appendChild(imageTag);
        anchorTag.appendChild(textNode);
        childContainer.appendChild(anchorTag);
        }
        // Third image
        if(data.link3 != ""){
        var childContainer = document.createElement('div');
        childContainer.classList.add('toolbar-child');
        childContainer.setAttribute('id', 'child-' + i);
        parentContainer.appendChild(childContainer);
        var anchorTag = document.createElement('a');
        anchorTag.setAttribute('href', data.linkurl3);
        var imageTag = document.createElement('img');
        imageTag.setAttribute('src', data.link3);
        imageTag.setAttribute('alt', 'example image');
        var textNode = document.createTextNode(data.title3);
        anchorTag.appendChild(imageTag);
        anchorTag.appendChild(textNode);
        childContainer.appendChild(anchorTag);
        }
        // Fourth image
        if(data.link4 != ""){
        var childContainer = document.createElement('div');
        childContainer.classList.add('toolbar-child');
        childContainer.setAttribute('id', 'child-' + i);
        parentContainer.appendChild(childContainer);
        var anchorTag = document.createElement('a');
        anchorTag.setAttribute('href', data.linkurl4);
        var imageTag = document.createElement('img');
        imageTag.setAttribute('src', data.link4);
        imageTag.setAttribute('alt', 'example image');
        var textNode = document.createTextNode(data.title4);
        anchorTag.appendChild(imageTag);
        anchorTag.appendChild(textNode);
        childContainer.appendChild(anchorTag);
        }
        // fifth image
        if(data.link5 != ""){
        var childContainer = document.createElement('div');
        childContainer.classList.add('toolbar-child');
        childContainer.setAttribute('id', 'child-' + i);
        parentContainer.appendChild(childContainer);
        var anchorTag = document.createElement('a');
        anchorTag.setAttribute('href', data.linkurl5);
        var imageTag = document.createElement('img');
        imageTag.setAttribute('src', data.link5);
        imageTag.setAttribute('alt', 'example image');
        var textNode = document.createTextNode(data.title5);
        anchorTag.appendChild(imageTag);
        anchorTag.appendChild(textNode);
        childContainer.appendChild(anchorTag);
        }

        // sixth image
        if(data.link6 != ""){
        var childContainer = document.createElement('div');
        childContainer.classList.add('toolbar-child');
        childContainer.setAttribute('id', 'child-' + i);
        parentContainer.appendChild(childContainer);
        var anchorTag = document.createElement('a');
        anchorTag.setAttribute('href', data.linkurl6);
        var imageTag = document.createElement('img');
        imageTag.setAttribute('src', data.link6);
        imageTag.setAttribute('alt', 'example image');
        var textNode = document.createTextNode(data.title6);
        anchorTag.appendChild(imageTag);
        anchorTag.appendChild(textNode);
        childContainer.appendChild(anchorTag);
        }
    } else {
        const element = document.getElementById('container-toolbar');
        if (element) {
            element.style.display = 'none';
        }
    }
}
Get_Data();




