import React from 'react'
import { useAuthenticatedFetch } from "../hooks"
const PricingPlan = () => {
    const fetch = useAuthenticatedFetch();
    const handlePlan=async ()=>{
        const response = await fetch("/api/select-plan");
        const data = await response.json();
        console.log(data)
    }
  return (
    <div>
      <button onClick={handlePlan}>Fetch Price</button>
    </div>
  )
}

export default PricingPlan
