import {TextStyle, Layout, MediaCard, VideoThumbnail} from '@shopify/polaris';
import React from 'react';

export default function WalkThrough() {
  return (

    <Layout style={{display:"flex", justifyContent:"center", alignItems:"center"}}>
        <Layout.Section>
        <span style={{fontSize:16, marginTop:10, paddingTop:100}}><br/>The video has been added to demonstrate the functioning of a Sticky Toolbar App
        </span>
        </Layout.Section>
        <Layout.Section>
    {/* <video  height="240" width="580"controls>
         <source src="https://www.youtube.com/embed/IcUVjjiYIws" />
    </video> */}
    <iframe width="580" height="280" src="https://www.youtube.com/embed/IcUVjjiYIws" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowFullScreen></iframe>
        </Layout.Section>
    </Layout>
  );
}