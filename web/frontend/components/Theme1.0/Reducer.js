export const reducer = (state, action) => {
    switch (action.type) {
      case 'UPDATE_FIELD':
        return {
          ...state,
          [action.field]: action.value
        };
      case 'SUBMIT_FORM':
        // Submit form data to server
        return state;
      default:
        return state;
    }
  };