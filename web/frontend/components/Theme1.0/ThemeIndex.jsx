import React, { useReducer, useCallback, useState } from "react";
import {
  Form,
  FormLayout,
  ButtonGroup,

  Checkbox,
  TextField,
  Button,
  Layout,
  Page,
  Toast,
  Frame,

} from "@shopify/polaris";
import "./style.css";
import { useAuthenticatedFetch } from "../../hooks";
import { reducer } from "./Reducer";
import { InitialArgs } from "./InitialArgs";
import ScriptTag from "./ScriptTag";
const ThemeIndex = () => {
  const [state, dispatch] = useReducer(reducer, InitialArgs);
  const [language, setLanguage] = useState("English");
  const [active, setActive] = useState(false);
  const fetch = useAuthenticatedFetch();
  const [activebtn , setActiveBtn] = useState(0);

  const translations = {

    english: {
      title: "Title",
      imageLink: "Image link",
      linkUrl: "Link URL",
      button: "submit"

    },

    italian: {
      title: "Titolo",
      imageLink: "Link immagine",
      linkUrl: "URL collegamento",
      button: "Presentare"

    },

  }


  const handleInputChanges = (e) => {

    const { id, value } = e.target;
    const translatedNames = translations[language][id] || id;
    dispatch({ type: "UPDATE_FIELD", field: translatedNames, value });
  };
 
  const handleEnglishClick = useCallback(() => {
    setLanguage("english");
    setActiveBtn(0);
  }, []);

  const handleItalianClick = useCallback(() => {
    setLanguage("italian");
    setActiveBtn(1);
  }, []);





  const toggleActive = useCallback(() => setActive((active) => !active), []);

  const toastMarkup = active ? (
    <Toast content="Saved Successfully" onDismiss={toggleActive} />
  ) : null;

  const { title1, link1, url1,
    title2, link2, url2,
    title3, link3, url3,
    title4, link4, url4,
    title5, link5, url5,
    title6, link6, url6 } = state
  const handleInputChange = (e) => {
    const { name, value } = e.target;
    dispatch({ type: "UPDATE_FIELD", field: name, value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const response = await fetch("/api/themeformdata", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        title1, link1, url1,
        title2, link2, url2,
        title3, link3, url3,
        title4, link4, url4,
        title5, link5, url5,
        title6, link6, url6
      })
    })
  };

  return (
    <Page>
      <Layout>
        <Layout.Section>
          <div style={{ padding: "0 20px 0px 20px" }}>
            <ScriptTag />

            <ButtonGroup segmented>
              <Button 
               pressed = {activebtn===0}
              onClick={handleEnglishClick}>English</Button>

              <Button 
               pressed = {activebtn===1}
               onClick={handleItalianClick}>Italian</Button>
            </ButtonGroup>
            <br />

            {language === 'english' ? (

              <span>
                To customize the appearance of Theme 1.0, there are three key elements
                that need to be added an icon, URL, Image Link, and Title. These elements can be
                added to the theme by filling this form. Once added, the changes can be
                saved by clicking the "submit" button, which will update the appearance
                of Theme 1.0 according to the new customizations.
              </span>


            ) : (

              <span>
                Per personalizzare l'aspetto del tema 1.0, ci sono tre elementi chiave
                che devono essere aggiunti: un'icona, un URL, un collegamento immagine e un titolo. Questi elementi possono essere
                aggiunto al tema compilando questo modulo. Una volta aggiunte, le modifiche possono essere
                salvato facendo clic sul pulsante "invia", che aggiornerà l'aspetto
                del Tema 1.0 secondo le nuove personalizzazioni.
              </span>

            )


            }
          </div>

          <Frame>
            <Form onSubmit={handleSubmit}>
              <FormLayout>
                <div class="toolbar-form-container">
                  <div class="signle-toolbar-container">

                    <label htmlFor="title1">
                      {language === "italian"
                        ? translations.italian.title
                        : "Title 1"}
                    </label>
                    <input
                      type="text"
                      name="title1"
                      value={title1}
                      onChange={handleInputChanges}
                      autoComplete="off"
                    />
                    <label htmlFor="">
                      {language === "italian"
                        ? translations.italian.imageLink
                        : "Image Link 1"}
                    </label>
                    <input
                      type="text"
                      name="link1"
                      value={link1}
                      onChange={handleInputChanges}
                      autoComplete="off"
                    />
                    <label htmlFor="linkurl1">
                      {language === "italian"
                        ? translations.italian.linkUrl
                        : "Link Url 1"}
                    </label>
                    <input
                      type="text"
                      name="url1"
                      value={url1}
                      onChange={handleInputChanges}
                      autoComplete="off"
                    />
                    {/* <input class="input-file" type="file"/> */}
                  </div>
                  <div class="signle-toolbar-container">

                    <label htmlFor="title2">
                      {language === "italian"
                        ? translations.italian.title
                        : "Title 2"}
                    </label>

                    <input
                      type="text"
                      name="title2"
                      value={title2}
                      onChange={handleInputChanges}
                      autoComplete="off"
                    />

                    <label htmlFor="imagelink1">
                      {language === "italian"
                        ? translations.italian.imageLink
                        : "Image Link 1"}
                    </label>

                    <input
                      type="text"
                      name="link2"
                      value={link2}
                      onChange={handleInputChanges}
                      autoComplete="off"
                    />
                    <label htmlFor="linkurl2">
                      {language === "italian"
                        ? translations.italian.linkUrl
                        : "Link Url 2"}
                    </label>
                    <input
                      type="text"
                      name="url2"
                      value={url2}
                      onChange={handleInputChanges}
                      autoComplete="off"
                    />

                    {/* <input class="input-file" type="file"/> */}
                  </div>

                  <div class="signle-toolbar-container">
                    <label htmlFor="title3">
                      {language === "italian"
                        ? translations.italian.title
                        : "Title 3"}
                    </label>
                    <input
                      type="text"
                      name="title3"
                      value={title3}
                      onChange={handleInputChanges}
                      autoComplete="off"
                    />

                    <label htmlFor="imagelink1">
                      {language === "italian"
                        ? translations.italian.imageLink
                        : "Image Link 3"}
                    </label>

                    <input
                      type="text"
                      name="link3"
                      value={link3}
                      onChange={handleInputChanges}
                      autoComplete="off"
                    />
                    <label htmlFor="linkurl3">
                      {language === "italian"
                        ? translations.italian.linkUrl
                        : "Link Url 3"}
                    </label>
                    <input
                      type="text"
                      name="url3"
                      value={url3}
                      onChange={handleInputChanges}
                      autoComplete="off"
                    />

                    {/* <input class="input-file" type="file"/> */}
                  </div>

                  <div class="signle-toolbar-container">

                  <label htmlFor="title4">
                    {language === "italian"
                      ? translations.italian.title
                      : "Title 4"}
                  </label>

                  <input
                    type="text"
                    name="title4"
                    value={title4}
                    onChange={handleInputChanges}
                    autoComplete="off"
                  />

                  <label htmlFor="imagelink4">
                    {language === "italian"
                      ? translations.italian.imageLink
                      : "Image Link 4"}
                  </label>

                  <input
                    type="text"
                    name="link4"
                    value={link4}
                    onChange={handleInputChanges}
                    autoComplete="off"
                  />

                  <label htmlFor="linkurl4">
                    {language === "italian"
                      ? translations.italian.linkUrl
                      : "Link Url 4"}
                  </label>

                  <input
                    type="text"
                    name="url4"
                    value={url4}
                    onChange={handleInputChanges}
                    autoComplete="off"
                  />

                  {/* <input class="input-file" type="file"/> */}
                </div>

                <div class="signle-toolbar-container">
                  <label htmlFor="title5">
                    {language === "italian"
                      ? translations.italian.title
                      : "Title 5"}
                  </label>
                  <input
                    type="text"
                    name="title5"
                    value={title5}
                    onChange={handleInputChanges}
                    autoComplete="off"
                  />

                  <label htmlFor="imagelink5">
                    {language === "italian"
                      ? translations.italian.imageLink
                      : "Image Link 5"}
                  </label>
                  <input
                    type="text"
                    name="link5"
                    value={link5}
                    onChange={handleInputChanges}
                    autoComplete="off"
                  />

                  <label htmlFor="linkurl5">
                    {language === "italian"
                      ? translations.italian.linkUrl
                      : "Link Url 5"}
                  </label>
                  <input
                    type="text"
                    name="url5"
                    value={url5}
                    onChange={handleInputChanges}
                    autoComplete="off"
                  />

                  {/* <input class="input-file" type="file"/> */}
                </div>

                <div class="signle-toolbar-container">
                  <label htmlFor="title6">
                    {language === "italian"
                      ? translations.italian.title
                      : "Title 6"}
                  </label>
                  <input
                    type="text"
                    name="title6"
                    value={title6}
                    onChange={handleInputChanges}
                    autoComplete="off"
                  />

                  <label htmlFor="imagelink6">
                    {language === "italian"
                      ? translations.italian.imageLink
                      : "Image Link 6"}
                  </label>

                  <input
                    type="text"
                    name="link6"
                    value={link6}
                    onChange={handleInputChanges}
                    autoComplete="off"
                  />

                  <label htmlFor="linkurl6">
                    {language === "italian"
                      ? translations.italian.linkUrl
                      : "Link Url 6"}
                  </label>
                  <input
                    type="text"
                    name="url6"
                    value={url6}
                    onChange={handleInputChanges}
                    autoComplete="off"
                  />

                  {/* <input class="input-file" type="file"/> */}
                </div>
                
                  <br></br>
                <div style={{ display: "flex" }}>
                  <Button submit onClick={toggleActive} primary>{

                      language === "italian"
                      ? translations.italian.button
                      : "Submit"

                  }</Button>
                  {toastMarkup}
                </div>
                </div>

              </FormLayout>
            </Form>
          </Frame>

        </Layout.Section>
      </Layout>
    </Page>
  );
};
export default ThemeIndex;


