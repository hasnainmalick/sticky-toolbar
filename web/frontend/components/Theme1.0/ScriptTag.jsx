import React,{useEffect, useCallback} from "react";
import { Button } from "@shopify/polaris";
import { useAuthenticatedFetch } from "../../hooks";


const ScriptTag = () => {
    const fetch = useAuthenticatedFetch();
    const handleCheck = useCallback(async () => {
  const response = await fetch("/api/theme");
  const data = await response.json();
}, []);

useEffect(() => {
    handleCheck();
  
}, [handleCheck]); 
    

    return(
        <div style={{padding:"10px 0"}}>
             {/* <Button onClick={handleCheck}>Inject Script Tag</Button> */}
        </div>

    )
}
export default ScriptTag
