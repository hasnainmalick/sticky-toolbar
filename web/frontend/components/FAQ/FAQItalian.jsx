import React from "react";
import { DescriptionList, TextStyle, Layout, Page } from "@shopify/polaris";

export default function FAQItalian(){
        return (
            <div>
              <Page>
                <Layout>
                    {/* Q1 */}
                  <Layout.Section>
                    <TextStyle>
                      <strong>D: Come si aggiunge un'icona alla barra degli strumenti adesiva?</strong>
                      <br />
                      R: Puoi aggiungere un'icona alla barra degli strumenti adesiva facendo clic sul pulsante "Aggiungi immagine". Se scegli di aggiungere un'icona tramite un collegamento, dovrai inserire l'URL dell'immagine che desideri
desidera che l'icona si colleghi a. Se scegli di caricare un'immagine, puoi selezionare un file immagine dal tuo computer.

                    </TextStyle>
                  </Layout.Section>
                  <br />
                  {/* Q2 */}
                  <Layout.Section>
                  <TextStyle>
                    <strong>D: Posso personalizzare il titolo di un'icona? </strong>
                    <br />
                    R: Sì, puoi personalizzare il titolo di un'icona facendo clic su icona e modificando il TextStyle che appare accanto ad esso.

                  </TextStyle>
                  </Layout.Section>
                  <br />
                  {/* Q3 */}
                  <Layout.Section>
                  <TextStyle>
                    <strong> D: Cosa succede quando clicco su un'icona?</strong> <br />
                    R: Quando fai clic su un'icona, verrai indirizzato alla pagina Web associata a tale icona. Se l'URL è per una pagina web esterna, si aprirà in una nuova scheda in modo che tu non esca dalla tua pagina corrente. Se l'URL è per una pagina all'interno del tuo Shopify store, navigherà all'interno della stessa scheda.
                  </TextStyle>
                  </Layout.Section>
                  <br />
               
                  {/* Q5 */}
                  <Layout.Section>
                  <TextStyle>
                    <strong> D: Quante icone posso aggiungere alla mia barra degli strumenti adesiva?</strong>
                    <br />
                    R: Ci sono 6 icone che puoi aggiungere alla tua barra degli strumenti adesiva.
                  </TextStyle>
                  </Layout.Section>
                  <br />
                  {/* Q6 */}
                  <Layout.Section>
                  <TextStyle>
                    <strong>
                    D: Posso modificare la dimensione delle icone sulla barra degli strumenti adesiva?

                    </strong>
                    <br />
                    R: La dimensione delle icone sulla barra degli strumenti fissa è determinata dalle dimensioni delle altre icone sulla barra degli strumenti. Se carichi il tuo immagine per un'icona, l'immagine verrà automaticamente ridimensionata per adattarsi alle dimensioni delle altre icone.
                  </TextStyle>
                  </Layout.Section>
        
                  {/* Q7 */}
                  <Layout.Section>
                    <TextStyle>
                      <strong>
                      D: Come posso aggiungere collegamenti nativi alle sezioni o alle pagine?

                      </strong><br/>
                    <DescriptionList
                          items={[
                            {
                              term: 'Prodotti',
                              description:
                                '/products',
                            },
                            {
                              term: 'Carrello',
                              description:
                                '/cart',
                            },
                            {
                              term: 'Catalogare',
                              description:
                                '/collections',
                            },
                            {
                              term: 'Contatto',
                              description:
                                '/pages/contact',
                            },
                            {
                              term: 'Ricerca',
                              description:
                                '/search',
                            }
                            ]}/>
                         
                    </TextStyle>
                  </Layout.Section>
                  {/* {Q8} */}
                  <Layout.Section>
                    <TextStyle>
                    <strong>   D: In quale formato devono essere caricate le icone? </strong>
                            <br/>
                         
                            R: Se stai importando un'immagine da un link esterno, puoi utilizzare il formato JPG o PNG. Se stai caricando icone, il formato dovrebbe essere SVG.

                    
                    </TextStyle>
                  </Layout.Section>
                </Layout>
              </Page>
            </div>
            
          );
}
