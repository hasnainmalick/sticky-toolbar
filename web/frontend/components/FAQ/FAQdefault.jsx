import React from "react";
import { DescriptionList, TextStyle, Layout, Page } from "@shopify/polaris";

export default function FAQdefault(){
        return (
            <div>
              <Page>
                <Layout>
                    {/* Q1 */}
                  <Layout.Section>
                    <TextStyle>
                      <strong>Q: How do I add an icon to my sticky toolbar?</strong>
                      <br />
                      A: You can add an icon to your sticky toolbar by clicking the "Add
                      Image" button. If you choose to add an icon
                      through a link, you'll need to enter the URL of the image you
                      want the icon to link to. If you choose to upload an image, you
                      can select an image file from your computer.
                    </TextStyle>
                  </Layout.Section>
                  <br />
                  {/* Q2 */}
                  <Layout.Section>
                  <TextStyle>
                    <strong>Q: Can I customize the title of an icon? </strong>
                    <br />
                    A: Yes, you can customize the title of an icon by clicking on the
                    icon and editing the TextStyle that appears next to it.
                  </TextStyle>
                  </Layout.Section>
                  <br />
                  {/* Q3 */}
                  <Layout.Section>
                  <TextStyle>
                    <strong> Q: What happens when I click on an icon?</strong> <br />
                    A: When you click on an icon, you will be taken to the webpage that
                    is associated with that icon. If the URL is for an external webpage,
                    it will open in a new tab so that you don't navigate away from your
                    current page. If the URL is for a page within your own Shopify
                    store, it will navigate within the same tab.
                  </TextStyle>
                  </Layout.Section>
                  <br />
               
                  {/* Q5 */}
                  <Layout.Section>
                  <TextStyle>
                    <strong> Q: How many icons can I add to my sticky toolbar?</strong>
                    <br />
                    A: There is 6 number of icons you can add to your
                    sticky toolbar.
                  </TextStyle>
                  </Layout.Section>
                  <br />
                  {/* Q6 */}
                  <Layout.Section>
                  <TextStyle>
                    <strong>
                      {" "}
                      Q: Can I change the size of the icons on my sticky toolbar?{" "}
                    </strong>
                    <br />
                    A: The size of the icons on your sticky toolbar is determined by the
                    dimensions of the other icons on the toolbar. If you upload your own
                    image for an icon, the image will be automatically resized to fit
                    the dimensions of the other icons.
                  </TextStyle>
                  </Layout.Section>
        
                  {/* Q7 */}
                  <Layout.Section>
                    <TextStyle>
                      <strong>
                        Q: How can I add native links to the sections or pages?
                      </strong><br/>
                    <DescriptionList
                          items={[
                            {
                              term: 'Products',
                              description:
                                '/products',
                            },
                            {
                              term: 'Cart',
                              description:
                                '/cart',
                            },
                            {
                              term: 'Catalog',
                              description:
                                '/collections',
                            },
                            {
                              term: 'Contact',
                              description:
                                '/pages/contact',
                            },
                            {
                              term: 'Search',
                              description:
                                '/search',
                            }
                            ]}/>
                         
                    </TextStyle>
                  </Layout.Section>
                  {/* {Q8} */}
                  <Layout.Section>
                    <TextStyle>
                    <strong>Q: Which format the icons should be uploaded? </strong>
                            <br/>
                            A: If you are importing image from an external link, you can use JPG or PNG format.
                            If you are uploading icons, the format should be SVG.
                    
                    </TextStyle>
                  </Layout.Section>
                </Layout>
              </Page>
            </div>
            
          );
}
