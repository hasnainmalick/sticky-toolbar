import React from "react";
import {
  DescriptionList,
  TextStyle,
  Layout,
  Page,
  ButtonGroup,
  Button,
} from "@shopify/polaris";
import FAQdefault from "./FAQdefault";
import FAQItalian from "./FAQItalian";
import { Tabs } from "@shopify/polaris";
import { useState, useCallback } from "react";

export default function FAQTab() {
  const [selected, setSelected] = useState(0);

  const handleTabChange = useCallback(
    (selectedTabIndex) => setSelected(selectedTabIndex),
    []
  );

  const tabs = [
    {
      id: "English",
      content: "English",
      panelID: "English",
    },
    {
      id: "Italian",
      content: "Italian",
      panelID: "Italian",
    }
  ];

  return (
    <Tabs tabs={tabs} selected={selected} onSelect={handleTabChange}>
      {tabs[selected].id === "English" && <FAQdefault />}
      {tabs[selected].id === "Italian" && <FAQItalian />}
      
    </Tabs>
  );
}
