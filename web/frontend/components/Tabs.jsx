import {Badge, Stack, Tabs} from '@shopify/polaris';
import {useState, useCallback} from 'react';
import WalkThrough from './WalkThrough/WalkThrough'
import FAQTab from './FAQ/FAQTab';
import FAQdefault from './FAQ/FAQdefault';
import FAQItalian from './FAQ/FAQItalian';
import ThemeIndex from './Theme1.0/ThemeIndex';



export default function StickyTabs() {
  const [selected, setSelected] = useState(0);

  const handleTabChange = useCallback(
    (selectedTabIndex) => setSelected(selectedTabIndex),
    [],
  );

  const tabs = [
    {
      id: 'WalkThrough',
      content: 'Walk Through',
      panelID: 'WalkThrough',
    },
    {
      id: 'FAQ',
      content: 'FAQ',
      panelID: 'FAQ',
      
    },
    {
      id:"Theme",
      content:"Theme 1.0",
      panelID:"Theme"
    }
   
  ];


  return (
    <Tabs tabs={tabs} selected={selected} onSelect = {handleTabChange}>
        
           {tabs[selected].id === "FAQ" &&  <FAQTab/>}
           {tabs[selected].id === "WalkThrough" && <WalkThrough/>}
           {tabs[selected].id === "Theme" && <ThemeIndex />}
      
      </Tabs>

  );
  }

