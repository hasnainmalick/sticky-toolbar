import mysql from 'mysql2'
import * as dotenv from "dotenv";
dotenv.config();

const HOST = process.env.DB_HOST
const USER = process.env.DB_USER
const PASSWORD = process.env.DB_USER
const DATABASE = process.env.DB_NAME
// create the connection to database
const connection = mysql.createConnection({
    host: HOST,
    user: USER,
    password: PASSWORD,
    database: DATABASE
});

export async function ThemeForm(shop, title1, link1, url1,
    title2, link2, url2,
    title3, link3, url3,
    title4, link4, url4,
    title5, link5, url5,
    title6, link6, url6) {
    //   const results = await connection.promise()00.query(sql);
    let query = `INSERT INTO theme_form_data
      (shop, title1, link1, linkurl1,
        title2, link2, linkurl2,
        title3, link3, linkurl3,
        title4, link4, linkurl4,
        title5, link5, linkurl5,
        title6, link6, linkurl6 ) VALUES (?, ?, ?,?, ?,?,?, ?, ?,?, ?,?,?, ?, ?,?, ?,?,?);`

    // Creating queries
    connection.query(query, [shop, title1, link1, url1,
        title2, link2, url2,
        title3, link3, url3,
        title4, link4, url4,
        title5, link5, url5,
        title6, link6, url6], (err, rows) => {
            if (err) throw err;
            else {
                console.log("Successfully inserted Contest Form data");
            }
        });
}

export async function GetThemeData(shop) {
    let query = `select * from theme_form_data where shop='${shop}' order by id desc;`
    const data = await connection.promise().query(query);
    return data[0];
}
